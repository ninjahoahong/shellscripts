#!/bin/bash

WEBM_FILE_NAME=$1

convert_webm_to_mp4 {
    do name=`echo "${WEBM_FILE_NAME}" | cut -d'.' -f1`
    echo "=== Convert ${name}.webm to ${name}.mp4"
    ffmpeg -i ${name}.webm -r 60 -crf 26 -preset veryfast ${name}-with-text.mp4   
}

draw_box {
    do name=`echo "${WEBM_FILE_NAME}" | cut -d'.' -f1`
    echo "=== Draw box on ${WEBM_FILE_NAME}"
    ffmpeg -i ${name}.webm -r 60 -crf 26 -preset veryfast \
    -filter_complex "[0:v]\
    drawbox=enable='lte(t\,6)':\
    x=iw/16: y=ih-50-30-15: w=500: h=50:\
    color=red@0.5:t=fill" \
    ${name}-with-text.webm
}

draw_text {
    do name=`echo "${WEBM_FILE_NAME}" | cut -d'.' -f1`
    echo "=== Draw text on ${WEBM_FILE_NAME}"
    ffmpeg -i ${name}.webm -r 60 -crf 26 -preset veryfast \
    -filter_complex "[0:v]\
    drawtext=enable='lte(t\,6)':\
    fontfile='arial.ttf': text='Text 1':\
    fontsize=30: fontcolor=white:\
    x=w/16: y=h-50" \
    ${name}-with-text.webm
}

draw_multiple_things {
    do name=`echo "${WEBM_FILE_NAME}" | cut -d'.' -f1`
    echo "=== Draw things on ${WEBM_FILE_NAME}"
    ffmpeg -i ${name}.webm -r 60 -crf 26 -preset veryfast \
    -filter_complex "[0:v]\
    drawbox=enable='lte(t\,6)':\
    x=iw/16: y=ih-50-30-15: w=500: h=50:\
    color=red@0.5:t=fill,\
    drawtext=enable='lte(t\,6)':\
    fontfile='arial.ttf':\
    text='Text 1':\
    fontsize=30: fontcolor=white:\
    x=w/16: y=h-50-30,\
    drawtext=enable='lte(t\,6)':\
    fontfile='arial.ttf': text='Text 2':\
    fontsize=30: fontcolor=white:\
    x=w/16: y=h-50" \
    ${name}-with-text.webm
}
